import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class CounterService {

  // Counter
  counter: number;
  counterChange = new EventEmitter<number>();


  // Store data
  PERSISTENT_OBJECT_KEY:string = 'dataCounter';

  constructor() {
    this.checkCounterData();
  }

  plusOne(){
    console.log('Counter Service add one to counter');
    // We update counter
    this.counter++;
    // We emit counterChange event with new counter value
    this.counterChange.emit(this.counter);
    // Update stored version
    this.updateCounterData();
  }

  minusOne() {
    console.log('Counter Service substract one to counter')
    // We update counter
    this.counter--;
    // We emit counterChange event with new counter value
    this.counterChange.emit(this.counter);
    // Update stored version
    this.updateCounterData();
  }

  updateCounterData() {
    store.set(this.PERSISTENT_OBJECT_KEY, this.counter);
  }

  checkCounterData() {
    let storedData: any;

    storedData = store.get(this.PERSISTENT_OBJECT_KEY);

    if ( storedData ) {
      console.log('//DATA STORED');
      this.counter = storedData;
    }
    else {
      console.log('//NO DATA STORED');
      store.set(this.PERSISTENT_OBJECT_KEY, 0);
      this.counter = 0;
    }
  };

}
